'''testsuite for the entropy module'''
from collections import Counter
from unittest import TestCase

import cohtools.entropy as E


class TestShannonEntropy(TestCase):
    '''Test shannon_entropy'''
    def test_shannon_entropy_1(self):
        '''2^8 different values mean 8 bit entropy'''
        barr = bytearray(range(256))
        self.assertEqual(E.shannon_entropy(barr), 8.0)

    def test_shannon_entropy_2(self):
        '''1 repeated value means 0 bit of entropy'''
        barr = bytearray([42 for _ in range(256)])
        self.assertEqual(E.shannon_entropy(barr), 0.0)

    def test_shannon_entropy_3(self):
        '''regression test for large counts'''
        counts =Counter(
            { 0: 45210183,  1: 45210183,  2: 45210183,  3: 45210183,
              4: 45210183,  5: 45210183,  6: 45210182,  7: 45210182,
              8: 45210182,  9: 45210182, 10: 45210182, 11: 45210182,
             12: 45210182, 13: 45210182, 14: 45210182, 15: 45210182,
             16: 45210182, 17: 45210182, 18: 45210182, 19: 45210182,
             20: 45210182, 21: 45210182, 22: 45210182, 23: 45210182,
             24: 45210182, 25: 45210182, 26: 45210182, 27: 45210182,
             28: 45210182, 29: 45210182, 30: 45210182, 31: 45210182,
             32: 45210182, 33: 45210182, 34: 45210182, 35: 45210182,
             36: 45210182, 37: 45210182, 38: 45210182, 39: 45210182,
             40: 45210182, 41: 45210182, 42: 45210182, 43: 45210182,
             44: 45210182, 45: 45210182, 46: 45210182, 47: 45210182,
             48: 45210182, 49: 45210182, 50: 45210182, 51: 45210182,
             52: 45210182, 53: 45210182, 54: 45210182, 55: 45210182,
             56: 45210182, 57: 45210182, 58: 45210182, 59: 45210182,
             60: 45210182, 61: 45210182, 62: 45210182, 63: 45210182,
             64: 45210182, 65: 45210182, 66: 45210182, 67: 45210182,
             68: 45210182, 69: 45210182, 70: 45210182, 71: 45210182,
             72: 45210182, 73: 45210182, 74: 45210182, 75: 45210182,
             76: 45210182, 77: 45210182, 78: 45210182, 79: 45210182,
             80: 45210182, 81: 45210182, 82: 45210182, 83: 45210182,
             84: 45210182, 85: 45210182, 86: 45210182, 87: 45210182,
             88: 45210182, 89: 45210182, 90: 45210182, 91: 45210182,
             92: 45210182, 93: 45210182, 94: 45210182})
        self.assertTrue(0 <= E.shannon_entropy(counts) <= 8.0)


class TestChunkedEntropy(TestCase):
    '''Test chunked_entropy'''
    def test_chunked_entropy_1(self):
        '''1 0-bit chunk + 1 8-bit chunk should show as that'''
        barr = bytearray([42 for _ in range(256)] + [_ for _ in range(256)])
        self.assertEqual(E.chunked_entropy(barr, 256), [0.0, 8.0])

    def test_chunked_entropy_2(self):
        '''without a chunk_size, it should behave just like shannon_entropy'''
        barr = bytearray(range(256))
        self.assertEqual(E.chunked_entropy(barr), 8.0)
