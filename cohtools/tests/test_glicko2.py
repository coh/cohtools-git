import sys
import warnings
from unittest import TestCase
from cohtools.glicko import Glicko, Glicko2, LOSS, WIN


class TestGlicko2(TestCase):
    def setUp(self):
        self.G = Glicko2(tau=0.5)
        self.player = Glicko2.Rating.from_glicko(Glicko.Rating(1500, 200))
        self.opponents = [Glicko2.Rating.from_glicko(x) for x in
                          (Glicko.Rating(1400, 30), Glicko.Rating(1550, 100),
                           Glicko.Rating(1700, 300))]
        self.results = (WIN, LOSS, LOSS)

    def tearDown(self):
        del self.G
        del self.player
        del self.results
        del self.opponents

    def test_tau_warning(self):
        if sys.version_info >= (3, 2):
            with self.assertWarns(UserWarning):
                Glicko2(tau=2)
        else:
            with warnings.catch_warnings(record=True) as w:
                warnings.simplefilter("always")
                Glicko2(tau=2)
                self.assertEqual(len(w), 1)
                self.assertTrue(issubclass(w[-1].category, UserWarning))
                self.assertIn('tau', str(w[-1].message))

    def test_default_rating(self):
        r = self.G.Rating.default_rating()
        self.assertEqual(r.rating, 1500)
        self.assertEqual(r.deviation, 350)

    def test_from_glicko(self):
        self.assertEqual(round(self.opponents[0].mu, 4), -0.5756)
        self.assertEqual(round(self.opponents[0].phi, 4), 0.1727)
        self.assertEqual(round(self.opponents[1].mu, 4),  0.2878)
        self.assertEqual(round(self.opponents[1].phi, 4), 0.5756)
        self.assertEqual(round(self.opponents[2].mu, 4),  1.1513)
        self.assertEqual(round(self.opponents[2].phi, 4), 1.7269)

    def test_estimated_variance(self):
        '''this case differs from the one in the paper as the calculations
           are done in full precision.'''
        expected_results = [self.player.expected_result(x) for x in
                            self.opponents]
        nu = self.player._estimated_variance(self.opponents, expected_results)
        self.assertEqual(round(nu, 4), 1.779)  # paper: 1.7785

    def test_estimated_improvement(self):
        '''this case differs from the one in the paper as the calculations
           are done in full precision.'''
        expected_results = [self.player.expected_result(x) for x in
                            self.opponents]
        delta = self.player._estimated_improvement(self.results,
                                                   self.opponents,
                                                   expected_results)
        self.assertEqual(round(delta, 4), -0.4839)  # paper: -0.4834

    def test_new_volatility(self):
        expected_results = [self.player.expected_result(x) for x in
                            self.opponents]
        delta = self.player._estimated_improvement(self.results,
                                                   self.opponents,
                                                   expected_results)
        nu = self.player._estimated_variance(self.opponents, expected_results)
        tau = 0.5
        sigma_prime = self.player._new_volatility(delta2=delta ** 2,
                                                  phi2=self.player.phi ** 2,
                                                  nu=nu, tau=tau)
        self.assertEqual(sigma_prime, 0.05999)

    def test_g(self):
        gj = list(map(lambda x: round(x.g(), 4), self.opponents))
        self.assertEqual(gj, [0.9955, 0.9531, 0.7242])

    def test_expected_result(self):
        Ej = list(map(lambda x: round(self.player.expected_result(x), 3),
                      self.opponents))
        self.assertEqual(Ej, [0.639, 0.432, 0.303])

    def test_new_player_rating(self):
        res = self.player.new_player_rating(self.results, self.opponents, 0.5)
        self.assertEqual(round(res.mu, 4), -0.2069)
        self.assertEqual(round(res.phi, 4), 0.8722)
