"""Implements both the Glicko and Glicko-2 rating algorithms."""
from __future__ import division
import math
import warnings


LOSS = 0.0
"""lost the match against the opponent"""

DRAW = 0.5
"""drew the match against the opponent"""

WIN = 1.0
"""won the match against the opponent"""


class Glicko(object):
    """Implements the Glicko algorithm.

       ``c`` is a constant that determines how fast the uncertainty about a
       player's score will grow during times of inactivity.

       The default value is the one named in the paper where glicko is
       described in detail.
    """

    class Rating(object):
        """The Glicko rating for a single player"""
        MIN_RD = 30
        MAX_RD = 350
        Q = round(math.log(10) / 400.0, 7)
        C = 63.2

        def __init__(self, r, rd):
            self._r = r
            self._rd = rd

        @property
        def rating(self):
            return self._r

        @property
        def deviation(self):
            return self._rd

        @rating.setter
        def rating(self, val):
            self._r = val

        @deviation.setter
        def deviation(self, val):
            self._rd = max(min(val, self.MAX_RD), self.MIN_RD)

        @classmethod
        def default_rating(cls):
            """return a new Rating with the default values"""
            return cls(1500, cls.MAX_RD)

        def _update_deviation(self, t, c=C):
            self.deviation = min(math.sqrt(self.deviation ** 2 + c ** 2 * t),
                                 self.MAX_RD)

        def g(self):
            return 1.0 / math.sqrt(1 + 3 * self.Q ** 2 * self.deviation ** 2 /
                                   math.pi ** 2)

        def _deviation_squared(self, opponents, expected_results):
            return 1.0 / (self.Q ** 2 *
                          sum(map(lambda x: x[0].g() ** 2 * x[1] * (1 - x[1]),
                                  zip(opponents, expected_results))))

        @staticmethod
        def _performance(results, opponents, expected_results):
            return sum(map(lambda x: x[1].g() * (x[0] - x[2]),
                           zip(results, opponents, expected_results)))

        def expected_result(self, other):
            '''returns the expected outcome of playing against an opponent
               with a given rating and deviation.

               :param other: the rating and deviation of the opponent
               :type other: Rating
               :rtype: float

               .. note::
                   This value is not a probability that one will win a
                   particular game, but instead the expected average score
                   over a large amount of played games bewteen the same two
                   parties.
            '''
            return 1.0 / (1 + 10 ** (-other.g() *
                                     (self.rating - other.rating) / 400.0))

        def new_player_rating(self, results, opponents, t, c=C):
            '''update the rating but return the values in a new instance'''
            self._update_deviation(t, c)
            expected_results = [self.expected_result(opponent) for opponent in
                                opponents]
            d2 = self._deviation_squared(opponents, expected_results)
            r_prime = self.rating + self.Q / (1.0 / self.deviation ** 2 +
                                              1.0 / d2) * \
                self._performance(results, opponents, expected_results)
            rd_prime = math.sqrt(1.0 / (1.0 / self.deviation ** 2 + 1.0 / d2))

            return self.__class__(round(r_prime), round(rd_prime, 1))

        def update_player_rating(self, results, opponents):
            '''update the rating'''
            res = self.new_player_rating(results, opponents)
            self.rating = res.rating
            self.deviation = res.deviation

    def __init__(self, c=63.2):
        self._c = c

    @property
    def c(self):
        return self._c


class Glicko2(object):
    """Implements the Glicko-2 rating system with the stable algorithm."""

    class Rating(Glicko.Rating):
        """The Glicko-2 rating for a single player"""
        CONVERSION_FACTOR = 400.0 / math.log(10)
        DEFAULT_SIGMA = 0.06

        def __init__(self, mu_, phi_, sigma_=DEFAULT_SIGMA):
            self.mu = mu_
            self.phi = phi_
            self.volatility = sigma_

        @classmethod
        def from_glicko(cls, rating):
            """create a new Glicko-2 from a Glicko-1 Rating"""
            res = cls.default_rating()
            res.rating = rating.rating
            res.deviation = rating.deviation
            return res

        @classmethod
        def default_rating(cls):
            """return a new Rating with the default values"""
            return cls(0, 350 / cls.CONVERSION_FACTOR)

        @property
        def mu(self):
            return (self.rating - 1500) / self.CONVERSION_FACTOR

        @property
        def phi(self):
            return self.deviation / self.CONVERSION_FACTOR

        @mu.setter
        def mu(self, val):
            self.rating = self.CONVERSION_FACTOR * val + 1500

        @phi.setter
        def phi(self, val):
            self.deviation = self.CONVERSION_FACTOR * val

        def expected_result(self, other):
            '''returns the expected outcome of playing against an opponent
               with a given rating and deviation.

               :param other: the rating and deviation of the opponent
               :type other: Rating
               :rtype: float

               .. note::
                   This value is not a probability that one will win a
                   particular game, but instead the expected average score
                   over a large amount of played games bewteen the same two
                   parties.

            '''
            return 1.0 / (1 + math.exp(-other.g() * (self.mu - other.mu)))

        def _estimated_variance(self, opponents, expected):
            return 1.0 / (sum(map(lambda x: x[0].g() ** 2 * x[1] * (1 - x[1]),
                                  zip(opponents, expected))))

        def _estimated_improvement(self, results, opponents, expected):
            return self._estimated_variance(opponents, expected) * sum(map(
                lambda x: x[1].g() * (x[0] - x[2]),
                zip(results, opponents, expected)))

        def g(self):
            return 1.0 / math.sqrt(1 + 3 * self.phi ** 2 / math.pi ** 2)

        @staticmethod
        def _f_of_x(x, a, delta2, phi2, nu, tau):
            return (math.exp(x) * (delta2 - phi2 - nu - math.exp(x))) / \
                (2.0 * (phi2 + nu + math.exp(x)) ** 2) - (x - a) / tau ** 2

        @classmethod
        def _iterate(self, _A, _B, fA, fB, epsilon, delta2, phi2, nu, tau, a):
            while abs(_B - _A) > epsilon:
                _C = _A + (_A - _B) * fA / (fB - fA)
                fC = self._f_of_x(_C, a, delta2, phi2, nu, tau)
                if fC * fB < 0:
                    _A = _B
                    fA = fB
                else:
                    fA = fA / 2.0
                _B = _C
                fB = fC
            return _A

        def _new_volatility(self, delta2, phi2, nu, tau):
            epsilon = 10.0 ** -6
            a = math.log(self.volatility ** 2)
            _A = a
            if delta2 > phi2 + nu:
                _B = math.log(delta2 - phi2 - nu)
            else:
                k = 1
                while self._f_of_x(a - k * math.sqrt(tau ** 2), a, delta2,
                                   phi2, nu, tau) < 0:
                    k += 1
                _B = a - k * math.sqrt(tau ** 2)

            fA = self._f_of_x(_A, a, delta2, phi2, nu, tau)
            fB = self._f_of_x(_B, a, delta2, phi2, nu, tau)

            res = self._iterate(_A, _B, fA, fB, epsilon, delta2, phi2, nu,
                                tau, a)
            return math.floor(10 ** 5 * math.exp(res / 2.0)) / 10.0 ** 5

        def new_player_rating(self, results, opponents, tau):
            '''update the rating but return the values in a new instance'''
            if len(results) == 0:
                phi_star = math.sqrt(self.sigma ** 2 + self.phi ** 2)
                return self.__class__(self.mu, phi_star)

            expected_results = [self.expected_result(opponent) for opponent in
                                opponents]
            nu = self._estimated_variance(opponents, expected_results)
            delta = self._estimated_improvement(results, opponents,
                                                expected_results)
            sigma_prime = self._new_volatility(delta ** 2, self.phi ** 2,
                                               nu, tau)
            phi_star = math.sqrt(sigma_prime ** 2 + self.phi ** 2)

            phi_prime = 1.0 / math.sqrt(1.0 / phi_star ** 2 + 1.0 / nu)
            mu_prime = self.mu + phi_prime ** 2 * (delta / nu)

            return self.__class__(mu_prime, phi_prime)

    def __init__(self, tau):
        if not 0.3 <= tau <= 1.2:
            warnings.warn('tau outside recommended range')
        self.tau = tau
