"""Implements Shannon Entropy computation for files or chunks thereof.
"""
import collections
import math
import os
import os.path

# pylint: disable=F0401
from six.moves import xrange  # cannot be found by pylint 0.25.2


def shannon_entropy(data):
    """Compute the shannon entropy for the given data.

    :param data: the data, **must** be iterable and have a length.
    :rtype:      float in range [0,8]

    """
    freqs = collections.Counter(data)

    n = sum(freqs.values())
    sn = sum(map(lambda x: x * math.log(x, 2), freqs.values()))
    return math.log(n, 2) - sn / n


def chunked_entropy(data, chunk_size=None, sliding_window=False):
    """Compute the shannon entropy for each chunk_size bytes of the data.

    If chunk_size is None (the default), then it behaves the same as
    shannon_entropy.

    :param data:           the data, **must** be iterable.

    :param chunk_size:     if not None, then it must be the size in bytes for
                           each chunk.

    :param sliding_window: if True, then compute something that is the moving
                           shannon_entropy with a window of chunk_size bytes.
    :rtype:                list of float, each in range [0,8].

    """
    if not chunk_size:
        return shannon_entropy(data)

    chunk_entropies = []
    step_size = int(sliding_window) or chunk_size

    for _ in xrange(0, len(data), step_size):
        chunk_entropies.append(shannon_entropy(data[_:_ + chunk_size]))

    return chunk_entropies


def find_truecrypt_files(root_directory, minimal_filesize=1 * 1024 ** 2,
                         minimal_entropy=7.999, test_length=10 * 1024 ** 2,
                         logger=None):
    """Try to find truecrypt containers recursively inside a directory tree.

    :param root_directory:   the top directory that shall be (recursively)
                             searched.

    :param minimal_filesize: minimal filesize that shall be tested (default
                             1 MiByte).

    :param minimal_entropy:  minimal entropy for which to return a filename
                             (default 7.999 bits per byte which is large
                             enough to discard most merely compressed archives
                             ).

    :param test_length:      test only the first test_length bytes in order to
                             speed things up (default 10 MiByte).

    :param logger:           the logger to be used
    :type logger:            None or Logger
    :rtype:                  dict of all filenames (as key) whose entropies
                             (as value) are above the minimal entropy.

    """
    file_entropies = dict()
    for root, _, files in os.walk(root_directory):
        if logger:
            logger.info("scanning %s", root)

        for fil in files:
            filename = os.path.join(root, fil)
            try:
                stats = os.stat(filename)
                if stats.st_size < minimal_filesize:
                    continue
            except OSError as err:
                if logger:
                    logger.error("%s", err)
                continue

            try:
                with open(filename, 'rb') as infile:
                    tmp = shannon_entropy(infile.read(test_length))
                    if tmp >= minimal_entropy:
                        file_entropies[filename] = tmp
            except IOError as err:
                if logger:
                    logger.error("%s", err)
    return file_entropies
