Glicko Module API
=================


.. automodule:: cohtools.glicko
   :members:
   :member-order: bysource

