Entropy Module API
==================

.. automodule:: cohtools.entropy
   :members:
   :member-order: bysource
