from setuptools import setup, find_packages
from setuptools.command.test import test as TestCommand


class PyTest(TestCommand):
    def finalize_options(self):
        TestCommand.finalize_options(self)
        self.test_args = []
        self.test_suite = True

    def run_tests(self):
        #import here, cause outside the eggs aren't loaded
        import pytest
        pytest.main(self.test_args)


long_desc = '''cohtools is a collection of misc. functions that include, but
aren't limited to:

  * shannon entropy
'''

setup(
    author='coh',
    author_email='coh@co-hoppe.net',
    classifiers=[
        "Development Status :: 4 - Beta",
        "Intended Audience :: Developers",
        "Intended Audience :: Information Technology",
        "License :: OSI Approved :: MIT License",
        "Natural Language :: English",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 2.7",
        "Programming Language :: Python :: 3",
        "Topic :: Communications",
        "Topic :: Internet",
        "Topic :: Scientific/Engineering :: Information Analysis",
        "Topic :: Software Development :: Libraries :: Python Modules",
        "Topic :: Utilities",
    ],
    cmdclass={'test': PyTest},
    description='collection of miscellaneous functions',
    keywords="shannon entropy",
    license="MIT",
    long_description=long_desc,
    name='cohtools',
    packages=['cohtools', 'cohtools.tests'],
    install_requires=['six'],
    tests_require=['pytest', ],
    test_suite='cohtools.tests',
    url='https://bitbucket.org/coh/cohtools',
    use_2to3=True,
    version='0.2',
    zip_safe=False,
)
